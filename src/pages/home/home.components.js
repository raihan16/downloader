import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  PermissionsAndroid,
  Alert,
} from 'react-native';

import styles from './home.styles';
import Feather from 'react-native-vector-icons/Feather';
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: '',
    };
  }

  componentDidMount() {
    this.permission();
  }

  permission = async () => {
    try {
      const granted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      ]);
    } catch (err) {
      console.warn(err);
    }
  };

  validateYouTubeUrl() {
    let rjx =
      /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
    let isValid = rjx.test(this.state.url);
    if (isValid) {
      this.props.navigation.navigate('Download', {
        url: this.state.url,
        disable: true,
      });
    } else {
      Alert.alert(
        'WARNING',
        'URL not found',
        [
          {
            Text: 'cancel',
            onPress: () => console.log('cancel'),
            style: 'cancel',
          },
          {Text: 'ok', onPress: () => console.log('oke')},
        ],
        {cancelable: false},
      );
    }
  }

  handleChange = e => {
    this.setState({url: e.target.value});
  };

  render() {
    const disable = () => this.state.url.length < 1;
    return (
      <View style={styles.container}>
        <Image
          style={styles.image}
          source={require('../../../assets/images/logo.png')}
        />
        <TextInput
          placeholder="Enter URL"
          style={styles.input}
          value={this.state.url}
          onChangeText={url => this.setState({url})}
          onChange={this.handleChange}
        />
        <TouchableOpacity
          disabled={disable()}
          style={disable() ? styles.buttondisable : styles.button}
          onPress={() => {
            this.validateYouTubeUrl();
            this.youtube_parser;
          }}>
          <Text style={disable() ? styles.textdisable : styles.text}>
            Continue
          </Text>
          <Feather
            name="arrow-right"
            size={20}
            color={disable() ? '#9A9A9A' : '#FFF'}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
