import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 15,
    backgroundColor: '#FFF',
  },
  input: {
    borderWidth: 1,
    borderColor: '#E4E9F2',
    padding: 10,
    borderRadius: 10,
    width: '95%',
    height:42,
    backgroundColor: '#F7F9FC',
    marginTop: 40,
    paddingHorizontal: 15,
    fontSize: 14,
  },
  errorMessage: {
    fontSize: 20,
    color: 'red',
    marginLeft: -80,
  },
  button: {
    flexDirection: 'row',
    backgroundColor: '#46A2BC',
    paddingVertical: 5,
    paddingHorizontal: 15,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 28,
    width:122,
    height:34,
  },
  buttondisable: {
    flexDirection: 'row',
    backgroundColor: '#EDEDED',
    paddingVertical: 5,
    paddingHorizontal: 15,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 28,
    width:122,
    height:34,
  },
  text: {
    fontFamily: 'Poppins-Regular',
    color: '#FFFFFF',
    fontSize: 14,
  },
  textdisable: {
    fontFamily: 'Poppins-Regular',
    color: '#9A9A9A',
    fontSize: 14,
  },
  image: {
    justifyContent: 'center',
    height: 139,
    width: 167,
    marginHorizontal: 104,
  },
});

export default styles;
