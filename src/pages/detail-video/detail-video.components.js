/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Text, FlatList, TouchableOpacity, Image} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import styles from './detail-video.styles';

export default class Detail_Video extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    let Video = [
      {
        img: require('../../../assets/images/video1.png'),
        title: 'SPIDER-MAN: NO WAY HOME',
        Tanggal: '01/08/2021',
        sizevideo: '2 MB',
      },
      {
        img: require('../../../assets/images/video2.png'),
        title: 'Star War',
        Tanggal: '01/08/2021',
        sizevideo: '2 MB',
      },
      {
        img: require('../../../assets/images/video3.png'),
        title: 'FINDING NEMO',
        Tanggal: '01/08/2021',
        sizevideo: '2 MB',
      },
      {
        img: require('../../../assets/images/video4.png'),
        title: 'star wars 2',
        Tanggal: '01/08/2021',
        sizevideo: '2 MB',
      },
      {
        img: require('../../../assets/images/video5.png'),
        title: 'adventure time',
        Tanggal: '01/08/2021',
        sizevideo: '2 MB',
      },
      {
        img: require('../../../assets/images/video1.png'),
        title: 'SPIDER-MAN: NO WAY HOME',
        Tanggal: '01/08/2021',
        sizevideo: '2 MB',
      },
      {
        img: require('../../../assets/images/video2.png'),
        title: 'Star War',
        Tanggal: '01/08/2021',
        sizevideo: '2 MB',
      },
      {
        img: require('../../../assets/images/video3.png'),
        title: 'FINDING NEMO',
        Tanggal: '01/08/2021',
        sizevideo: '2 MB',
      },
      {
        img: require('../../../assets/images/video4.png'),
        title: 'star wars 2',
        Tanggal: '01/08/2021',
        sizevideo: '2 MB',
      },
      {
        img: require('../../../assets/images/video5.png'),
        title: 'adventure time',
        Tanggal: '01/08/2021',
        sizevideo: '2 MB',
      },
    ];
    return (
      <View style={styles.Container}>
      <FlatList
        style={{}}
        data={Video}
        showsVerticalScrollIndicator={false}
        renderItem={({item, index}) => {
          return (
          <TouchableOpacity style={{paddingHorizontal: 15}}>
            <View style={styles.textView}>
              <Image source={item.img} style={styles.img} />
            <View style={styles.textView2}>
              <Text style={styles.textstyle}>{item.title}</Text>
              <Text style={styles.textstyle2}>{item.Tanggal}</Text>
            <View style={styles.iconsize}>
              <Text style={styles.textsize}>{item.sizevideo}</Text>
              <TouchableOpacity style={styles.icon}>
              <Feather
                name="more-horizontal"
                size={20}
                color="#8F9BB3"/>
              </TouchableOpacity>
            </View>
            </View>
            </View>
            <View style={styles.sparator} />
              </TouchableOpacity>
              );
            }}
          />
      </View>
      );
    }
  }
