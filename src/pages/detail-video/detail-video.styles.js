import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  textView: {
    flexDirection: 'row',
    marginVertical: 15,
    flexGrow: 1,
  },
  img: {
    width: 127,
    height: 83,
    borderRadius: 10,
  },
  textView2: {
    marginLeft: 10,
    flexGrow: 1,
  },
  iconsize: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexGrow: 1,
    marginTop: 15,
  },
  textstyle: {
    fontSize: 15,
    fontFamily: 'Poppins-Medium',
    fontWeight: '500',
    color: '#3A3A3A',
  },
  textstyle2: {
    fontSize: 13,
    fontFamily: 'Poppins-Regular',
    fontWeight: '400',
    color: '#9A9A9A',
  },
  textsize: {
    fontSize: 13,
    fontFamily: 'Poppins-Regular',
    fontWeight: '400',
    color: '#9A9A9A',
  },
  icon: {
    backgroundColor: '#F7F9FC',
    borderRadius: 3,
    width: 34,
    height: 20,
    alignItems: 'center',
  },
  sparator: {
    borderBottomColor: '#EDEDED',
    borderBottomWidth: 1,
    width: '90%',
    marginHorizontal: 20,
  },
});
export default styles;
