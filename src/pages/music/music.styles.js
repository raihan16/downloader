import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  safeview: {
    flex: 1,
  },
  container: {
    flex: 1,
  },
  image: {
    width: '100%',
    height: 226,
  },
  titlecontainer: {
    flexDirection: 'row',
    width: '100%',
    marginLeft: 15,
  },
  title: {
    color: '#3A3A3A',
    fontSize: 18,
    fontStyle: 'normal',
    fontWeight: '500',
    fontFamily: 'Poppins-Medium',
    width: 283,
    height: 48,
    top: 18,
    lineHeight: 24,
  },
  album: {
    color: '#6A6A6A',
    marginTop: 15,
    marginLeft: 15,
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
  },
  Icon: {
    marginTop: 23,
    marginLeft: 50,
  },
  titlecontainer2: {
    flexDirection: 'column',
    marginLeft: 15,
  },
  title2: {
    color: '#3A3A3A',
    fontSize: 14,
    fontStyle: 'normal',
    fontWeight: '500',
    fontFamily: 'Poppins-Medium',
    width: 219,
    height: 42,
    left: 10,
    top: 15,
    lineHeight: 21,
  },
  album2: {
    color: '#6A6A6A',
    marginLeft: 10,
    marginTop: 14,
    fontSize: 10,
    fontWeight: '400',
    fontFamily: 'Poppins-Regular',
  },
  button: {
    flexDirection: 'row',
    backgroundColor: '#46A2BC',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: 345,
    height: 34,
    top: 21,
    borderRadius: 20,
  },
  sparator: {
    borderBottomColor: '#EDEDED',
    borderBottomWidth: 1,

    width: '90%',
    marginHorizontal: 20,
  },
  listview: {
    marginTop: 41,
    left: 15,
  },
  img: {
    height: 54,
    width: 54,
    marginBottom: 15,
  },
  dataContainer: {
    flexDirection: 'column',
    paddingLeft: 10,
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: 500,
    color: '#3A3A3A',
    lineHeight: 24,
    top: 8,
  },
  Iconlist: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 90,
    top: 15,
  },
  title3: {
    fontFamily: 'Poppins-Medium',
    color: '#3A3A3A',
    fontSize: 14,
    fontWeight: '500',
  },
  subtitle: {
    color: '#9A9A9A',
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    letterSpacing: 0.005,
  },
});

export default styles;
