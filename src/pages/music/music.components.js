/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, ScrollView} from 'react-native';
import styles from './music.styles';
import Feather from 'react-native-vector-icons/Feather';
import LottieView from 'lottie-react-native';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import {SafeAreaView} from 'react-native-safe-area-context';

export default class Music extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <SafeAreaView style={styles.safeview}>
        <ParallaxScrollView
          backgroundColor="#FFFFFF"
          contentBackgroundColor="#FFFFFF"
          parallaxHeaderHeight={450}
          renderForeground={() => (
            <View>
              <View>
                <Image
                  source={require('../../../assets/images/music.png')}
                  style={styles.image}
                />
              </View>
              <View style={styles.titlecontainer}>
                <Text style={styles.title}>
                  The Chainsmokers & Coldplay -Something Just Like This
                </Text>
                <TouchableOpacity>
                  <Feather
                    name="heart"
                    size={18}
                    color={'#46A2BC'}
                    style={styles.Icon}
                  />
                </TouchableOpacity>
              </View>
              <View>
                <Text style={styles.album}>by Coldplay</Text>
              </View>
              <View style={{flexDirection: 'row', marginLeft: 15}}>
                <Image
                  source={require('../../../assets/images/music/music1.png')}
                  style={{
                    width: 54,
                    height: 54,
                    marginTop: 17,
                    borderRadius: 3,
                  }}
                />
                <View style={{flexDirection: 'column'}}>
                  <Text style={styles.title2}>
                    The Chainsmokers & Coldplay -Something Just Like This
                  </Text>
                  <Text style={styles.album2}>by Coldplay</Text>
                </View>
              </View>
              <View>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() =>
                    this.props.navigation.navigate('Detail_Music')
                  }>
                  <Feather name="play" size={20} color="#FFF" />
                  <Text
                    style={{
                      color: '#FFF',
                      fontFamily: 'Poppins-Regular',
                      marginVertical: 5,
                      marginHorizontal: 5,
                    }}>
                    Play
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          )}>
          <ScrollView>
            <View style={{height: 805, top: 5}}>
              <View style={styles.sparator} />
              <View style={{left: 15}}>
                <TouchableOpacity style={{flexDirection: 'row', marginTop: 20}}>
                  <View>
                    <Image
                      source={require('../../../assets/images/music/1.png')}
                      style={{width: 54, height: 54, borderRadius: 3}}
                    />
                  </View>
                  <View
                    style={{
                      left: 10,
                      justifyContent: 'center',
                      width: 76,
                      height: 40,
                    }}>
                    <Text style={styles.title3}>The Nights</Text>
                    <Text style={styles.subtitle}>Avicii</Text>
                  </View>
                  <View style={{left: 100}}>
                    <LottieView
                      style={{width: 14, height: 20, left: 30, top: 8}}
                      source={require('../../../assets/lottieView.json')}
                      autoPlay
                      loop
                    />
                  </View>
                  <TouchableOpacity style={{left: 85}}>
                    <Feather
                      name="heart"
                      size={18}
                      color={'#46A2BC'}
                      style={styles.Iconlist}
                    />
                  </TouchableOpacity>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', marginTop: 17}}>
                  <Image
                    source={require('../../../assets/images/music/2.png')}
                    style={{width: 54, height: 54, borderRadius: 3}}
                  />
                  <View
                    style={{
                      left: 10,
                      justifyContent: 'center',
                      width: 76,
                      height: 40,
                    }}>
                    <Text style={styles.title3}>It Will Rain</Text>
                    <Text style={styles.subtitle}>Bruno Mars</Text>
                  </View>
                  <TouchableOpacity style={{left: 105}}>
                    <Feather
                      name="heart"
                      size={18}
                      color={'#46A2BC'}
                      style={styles.Iconlist}
                    />
                  </TouchableOpacity>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', marginTop: 17}}>
                  <Image
                    source={require('../../../assets/images/music/3.png')}
                    style={{width: 54, height: 54, borderRadius: 3}}
                  />
                  <View
                    style={{
                      left: 10,
                      justifyContent: 'center',
                      width: 76,
                      height: 40,
                    }}>
                    <Text style={styles.title3}>Paradise</Text>
                    <Text style={styles.subtitle}>ColdPlay</Text>
                  </View>
                  <TouchableOpacity style={{left: 105}}>
                    <Feather
                      name="heart"
                      size={18}
                      color={'#46A2BC'}
                      style={styles.Iconlist}
                    />
                  </TouchableOpacity>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', marginTop: 17}}>
                  <Image
                    source={require('../../../assets/images/music/1.png')}
                    style={{width: 54, height: 54, borderRadius: 3}}
                  />
                  <View
                    style={{
                      left: 10,
                      justifyContent: 'center',
                      width: 76,
                      height: 40,
                    }}>
                    <Text style={styles.title3}>The Nights</Text>
                    <Text style={styles.subtitle}>Avicii</Text>
                  </View>
                  <TouchableOpacity style={{left: 105}}>
                    <Feather
                      name="heart"
                      size={18}
                      color={'#46A2BC'}
                      style={styles.Iconlist}
                    />
                  </TouchableOpacity>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', marginTop: 17}}>
                  <Image
                    source={require('../../../assets/images/music/3.png')}
                    style={{width: 54, height: 54, borderRadius: 3}}
                  />
                  <View
                    style={{
                      left: 10,
                      justifyContent: 'center',
                      width: 76,
                      height: 40,
                    }}>
                    <Text style={styles.title3}>Paradise</Text>
                    <Text style={styles.subtitle}>ColdPlay</Text>
                  </View>
                  <TouchableOpacity style={{left: 105}}>
                    <Feather
                      name="heart"
                      size={18}
                      color={'#46A2BC'}
                      style={styles.Iconlist}
                    />
                  </TouchableOpacity>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', marginTop: 17}}>
                  <Image
                    source={require('../../../assets/images/music/1.png')}
                    style={{width: 54, height: 54, borderRadius: 3}}
                  />
                  <View
                    style={{
                      left: 10,
                      justifyContent: 'center',
                      width: 76,
                      height: 40,
                    }}>
                    <Text style={styles.title3}>The Nights</Text>
                    <Text style={styles.subtitle}>Avicii</Text>
                  </View>
                  <TouchableOpacity style={{left: 105}}>
                    <Feather
                      name="heart"
                      size={18}
                      color={'#46A2BC'}
                      style={styles.Iconlist}
                    />
                  </TouchableOpacity>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', marginTop: 17}}>
                  <Image
                    source={require('../../../assets/images/music/3.png')}
                    style={{width: 54, height: 54, borderRadius: 3}}
                  />
                  <View
                    style={{
                      left: 10,
                      justifyContent: 'center',
                      width: 76,
                      height: 40,
                    }}>
                    <Text style={styles.title3}>Paradise</Text>
                    <Text style={styles.subtitle}>ColdPlay</Text>
                  </View>
                  <TouchableOpacity style={{left: 105}}>
                    <Feather
                      name="heart"
                      size={18}
                      color={'#46A2BC'}
                      style={styles.Iconlist}
                    />
                  </TouchableOpacity>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', marginTop: 17}}>
                  <Image
                    source={require('../../../assets/images/music/1.png')}
                    style={{width: 54, height: 54, borderRadius: 3}}
                  />
                  <View
                    style={{
                      left: 10,
                      justifyContent: 'center',
                      width: 76,
                      height: 40,
                    }}>
                    <Text style={styles.title3}>The Nights</Text>
                    <Text style={styles.subtitle}>Avicii</Text>
                  </View>
                  <TouchableOpacity style={{left: 105}}>
                    <Feather
                      name="heart"
                      size={18}
                      color={'#46A2BC'}
                      style={styles.Iconlist}
                    />
                  </TouchableOpacity>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', marginTop: 17}}>
                  <Image
                    source={require('../../../assets/images/music/3.png')}
                    style={{width: 54, height: 54, borderRadius: 3}}
                  />
                  <View
                    style={{
                      left: 10,
                      justifyContent: 'center',
                      width: 76,
                      height: 40,
                    }}>
                    <Text style={styles.title3}>Paradise</Text>
                    <Text style={styles.subtitle}>ColdPlay</Text>
                  </View>
                  <TouchableOpacity style={{left: 105}}>
                    <Feather
                      name="heart"
                      size={18}
                      color={'#46A2BC'}
                      style={styles.Iconlist}
                    />
                  </TouchableOpacity>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', marginTop: 17}}>
                  <Image
                    source={require('../../../assets/images/music/1.png')}
                    style={{width: 54, height: 54, borderRadius: 3}}
                  />
                  <View
                    style={{
                      left: 10,
                      justifyContent: 'center',
                      width: 76,
                      height: 40,
                    }}>
                    <Text style={styles.title3}>The Nights</Text>
                    <Text style={styles.subtitle}>Avicii</Text>
                  </View>
                  <TouchableOpacity style={{left: 105}}>
                    <Feather
                      name="heart"
                      size={18}
                      color={'#46A2BC'}
                      style={styles.Iconlist}
                    />
                  </TouchableOpacity>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', marginTop: 17}}>
                  <Image
                    source={require('../../../assets/images/music/3.png')}
                    style={{width: 54, height: 54, borderRadius: 3}}
                  />
                  <View
                    style={{
                      left: 10,
                      justifyContent: 'center',
                      width: 76,
                      height: 40,
                    }}>
                    <Text style={styles.title3}>Paradise</Text>
                    <Text style={styles.subtitle}>ColdPlay</Text>
                  </View>
                  <TouchableOpacity style={{left: 105}}>
                    <Feather
                      name="heart"
                      size={18}
                      color={'#46A2BC'}
                      style={styles.Iconlist}
                    />
                  </TouchableOpacity>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection: 'row', marginTop: 17}}>
                  <Image
                    source={require('../../../assets/images/music/1.png')}
                    style={{width: 54, height: 54, borderRadius: 3}}
                  />
                  <View
                    style={{
                      left: 10,
                      justifyContent: 'center',
                      width: 76,
                      height: 40,
                    }}>
                    <Text style={styles.title3}>The Nights</Text>
                    <Text style={styles.subtitle}>Avicii</Text>
                  </View>
                  <TouchableOpacity style={{left: 105}}>
                    <Feather
                      name="heart"
                      size={18}
                      color={'#46A2BC'}
                      style={styles.Iconlist}
                    />
                  </TouchableOpacity>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </ParallaxScrollView>
      </SafeAreaView>
    );
  }
}
