import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  gridView: {
    alignItems: 'center',
  },
  containeritem: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingHorizontal: 15,
    marginVertical: 15,
  },
  itemContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    height: 165,
    width: 165,
    backgroundColor: '#F8F9FD',
    borderRadius: 10,
  },
  itemName: {
    top: 10,
    fontSize: 18,
    color: '#3A3A3A',
    fontFamily: 'Poppins-SemiBold',
  },
  image: {
    width: 62,
    height: 53.51,
  },
});

export default styles;
