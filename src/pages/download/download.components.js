import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Modal,
} from 'react-native';
import styles from './download.styles';
import Feather from 'react-native-vector-icons/Feather';
import RBSheet from 'react-native-raw-bottom-sheet';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import ytdl from 'react-native-ytdl';
import SelectDropdown from 'react-native-select-dropdown';
import YoutubePlayer, {getYoutubeMeta} from 'react-native-youtube-iframe';
import {RNFFmpeg} from 'react-native-ffmpeg';
import RNFetchBlob from 'rn-fetch-blob';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import {ScrollView} from 'react-native-gesture-handler';

const quality = [
  {title: '320 kbit/s - Highest', itag: 'lowest'},
  {title: '128 kbit/s - Lowest ', itag: 'highest'},
];

export default class Download extends Component {
  constructor(props) {
    super(props);
    this.state = {
      artis: '',
      activeIndex: 0,
      DurationEnd: 1,
      DurationStart: 0,
      error: null,
      fullscreen: false,
      quality: null,
      status: null,
      isReady: false,
      isPLaying: true,
      isLooping: true,
      title: '',
      modalVisible: false,
      spinner: false,
      showVideo: true,
      status: null,
      modalsTrimer: false,
      trimer: null,
      quality: '',
      data: [],
      folder: '',
    };
  }
  componentDidMount() {
    var RNFS = require('react-native-fs');
    RNFS.readDir(RNFS.ExternalStorageDirectoryPath + '/Movies')
      .then(result => {
       console.log('GOT RESULT', result.filter(name => name.isDirectory(true)).map(filteredName => ( console.log(filteredName))))
        return Promise.all([RNFS.stat(result[0].path), result[0].path]);
      })

      .then(contents => {
        console.log(contents);
      })
      .catch(err => {
        console.log(err.message, err.code);
      });
  }

  makeDirectory() {
    var RNFS = require('react-native-fs');
    const directoryName = this.state.folder;
    const absolutePath = `/storage/emulated/0/Movies/${directoryName}`;
    RNFS.mkdir(absolutePath)
      .then(result => {
        console.log(result);
        this.setModalVisible(!this.state.modalVisible);
        alert('success');
      })
      .catch(err => {
        console.warn('err', err);
      });
  }

  readfolder() {
    var RNFS = require('react-native-fs');
    RNFS.readDir(RNFS.ExternalStorageDirectoryPath + '/Movies')
      .then(result => {
        this.RBSheet.open();
        console.log('GOT RESULT', this.setState({data: result}));

        return Promise.all([RNFS.stat(result[0].path), result[0].path]);
      })
      .then(contents => {
        console.log(contents);
      })
      .catch(err => {
        console.log(err.message, err.code);
      });
  }

  list = () => {
    return this.state.data.filter(name => name.isDirectory(true)).map((element, i) => {
      return (
        <View key={i} style={{width: 75, height: 100, margin: 10}}>
          <TouchableOpacity style={styles.setviewfolder}>
            <Image
              style={styles.imagesfile}
              source={require('../../../assets/images/file.png')}
            />
          </TouchableOpacity>
          <Text style={styles.textshet}>{element.name}</Text>
        </View>
      );
    });
  };

  setModalVisible = visible => {
    this.setState({modalVisible: !this.state.modalVisible});
  };

  multiSliderValuesChange = values => {
    this.setState({
      DurationStart: values[0],
      DurationEnd: values[1],
    });
  };

  youtube_parser() {
    var regExp =
      /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    var match = this.props.route.params.url.match(regExp);
    return match && match[7].length == 11 ? match[7] : false;
  }

  DownloadMusic = async () => {
    this.setState({spinner: true});
    const youtubeURL = this.props.route.params.url;
    let info = await ytdl.getInfo(youtubeURL);
    let format = ytdl.chooseFormat(info.formats, this.state.quality);
    console.log(format);
    let video = format.url;
    let dirs = RNFetchBlob.fs.dirs;
    console.log(dirs.MovieDir);
    RNFetchBlob.config({
      addAndroidDownloads: {
        useDownloadManager: true,
        notification: true,
        path: dirs.MovieDir + '/' + this.state.title + '.mp3',
        title: this.state.title,
        mime: format.type,
      },
      fileCache: true,
    })
      .fetch('GET', video)
      .then(res => {
        this.setState({trimer: res.path()});
        console.log('**************');
        this.setState({spinner: false});
      })
      .catch(err => {
        this.setState({spinner: false});
        // error handling ...
      });
  };

  DownloadVideo = async () => {
    this.setState({spinner: true});
    const youtubeURL = this.props.route.params.url;
    let info = await ytdl.getInfo(youtubeURL);
    let format = ytdl.chooseFormat(info.formats, {quality: this.state.quality});
    console.log(format);
    let video = format.url;
    let dirs = RNFetchBlob.fs.dirs;
    console.log(dirs.MovieDir);
    RNFetchBlob.config({
      addAndroidDownloads: {
        useDownloadManager: true,
        notification: true,
        path: dirs.MovieDir + '/' + this.state.title,
        title: this.state.title,
        mime: format.type,
      },
      fileCache: true,
    })
      .fetch('GET', video)
      .then(res => {
        this.setState({spinner: false});
        let startTime = this.ConvertTime(this.state.DurationStart);
        let stopTime = this.ConvertTime(this.state.DurationEnd);
        this.setState({trimer: res.path()});
        console.log('**************');
        RNFFmpeg.executeWithArguments([
          '-i',
          this.state.trimer,
          '-ss',
          `${startTime}`,
          '-t',
          `${stopTime}`,
          '-codec',
          'copy',
          `${this.state.trimer}.mp4`,
        ]).then(result => {
          console.log('FFmpeg process exited  ' + result);
          if (!result) {
            this.setState({message: 'Splitted successfully'});
          } else {
            this.setState({message: 'Spliting failed'});
          }
        });
        console.log('The file is save to ', res.path());
        this.setState({modalsTrimer: true});
      })
      .catch(err => {
        this.setState({spinner: false});
        // error handling ...
      });
  };

  trimLocalVideo() {
    let dirs = RNFetchBlob.fs.dirs;
    console.log(dirs.MovieDir);
    let startTime = this.ConvertTime(this.state.DurationStart);
    let stopTime = this.ConvertTime(this.state.DurationEnd);
    let video = this.state.trimer;
    let number = Math.floor(Math.random() * 10) + 1;
    RNFFmpeg.executeWithArguments([
      '-i',
      video,
      '-ss',
      `${startTime}`,
      '-t',
      `${stopTime}`,
      '-codec',
      'copy',
      `${video}-trim-${number}.mp4`,
    ]).then(result => {
      console.log('FFmpeg process exited  ' + result);
      if (!result) {
        this.setState({message: 'Splitted successfully'});
        this.setState({modalsTrimer: true});
      } else {
        this.setState({message: 'Spliting failed'});
      }
    });
  }

  deleteVIdeo() {
    let dirs = RNFetchBlob.fs.dirs;
    console.log(dirs.MovieDir);
    let video = this.state.trimer;
    RNFetchBlob.fs
      .unlink(video)
      .then(() => console.log('file is deleted'))
      .catch(err => console.log('err', err));
  }

  cekmedia() {
    let media = this.state.trimer;
    if (media) {
      this.trimLocalVideo();
    } else {
      this.DownloadVideo();
    }
  }

  ConvertTime = t => {
    const digit = n => (n < 10 ? `0${n}` : `${n}`);
    const sec = digit(Math.floor(t % 60));
    const min = digit(Math.floor((t / 60) % 60));
    const hr = digit(Math.floor((t / 3600) % 60));
    return hr + ':' + min + ':' + sec;
  };

  render() {
    return (
      <View style={styles.container}>
        <Spinner
          animation="fade"
          visible={this.state.spinner}
          textContent={'Downloading...'}
          textStyle={styles.spinnerTextStyle}
          customIndicator={
            <LottieView
              style={{width: 150, height: 150}}
              source={require('../../../assets/download.json')}
              autoPlay
              loop
            />
          }
        />
        <Modal
          visible={this.state.modalsTrimer}
          transparent={true}
          animationType="fade">
          <View style={styles.centeredView}>
            <View style={styles.modalDownload}>
              <LottieView
                style={{width: 150, height: 150}}
                source={require('../../../assets/success.json')}
                autoPlay
                loop
              />
              <Text style={styles.textDownloadSucsess}>Success</Text>
              <View style={styles.buttonmodals}>
                <TouchableOpacity
                  style={styles.buttonTrimeragain}
                  onPress={() => this.setState({modalsTrimer: false})}>
                  <Text style={styles.textTrim}>Trim Again</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.buttonHideDownload}
                  onPress={() => {
                    this.deleteVIdeo();
                    this.setState({modalsTrimer: false});
                    this.props.navigation.goBack(null);
                  }}>
                  <Text style={styles.textModalsDD}>Done</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
        <View style={styles.youtubePlayer}>
          <YoutubePlayer
            videoId={this.youtube_parser()}
            ref={ref => (this.YouTube = ref)}
            play={false}
            loop={this.state.isLooping}
            showinfo={true}
            controls={1}
            fullscreen={this.state.fullscreen}
            width={'100%'}
            height={'100%'}
            onError={e => console.log(e)}
            onReady={e => {
              this.setState({isReady: true});
              this.YouTube.getDuration()
                .then(duration => {
                  this.setState({DurationEnd: duration});
                })
                .catch(errorMessage => {
                  console.log(errorMessage);
                });
              getYoutubeMeta(this.youtube_parser()).then(meta => {
                this.setState({title: meta.title});
              });
              getYoutubeMeta(this.youtube_parser()).then(meta => {
                this.setState({artis: meta.author_name});
              });
            }}
            onChangeQuality={e => {
              this.setState({quality: e.quality});
              console.log(e.quality);
            }}
            onChangeFullscreen={e => {
              this.setState({fullscreen: e.isFullscreen});
            }}
            onProgress={e => {
              console.log(e);
            }}
          />
        </View>
        <View style={styles.optimaizing}>
          <Text style={styles.text}>From : </Text>
          <Text style={styles.placeholder}>
            {this.ConvertTime(this.state.DurationStart)}
          </Text>
          <Text style={styles.textto}>To : </Text>
          <Text style={styles.placeholder}>
            {this.ConvertTime(this.state.DurationEnd)}
          </Text>
        </View>
        <View style={styles.settingslide}>
          <MultiSlider
            values={[this.state.DurationStart, this.state.DurationEnd]}
            sliderLength={345}
            onValuesChange={this.multiSliderValuesChange}
            min={this.state.DurationStart}
            max={this.state.DurationEnd}
            step={1}
            allowOverlap
            snapped
            selectedStyle={styles.selectedStyle}
            unselectedStyle={styles.unselectedStyle}
            pressedMarkerStyle={styles.pressedMarkerStyle}
            markerStyle={styles.markerslide}
          />
        </View>
        <View style={styles.format}>
          <Text style={styles.textformat}>Format</Text>
          <View>
            <TouchableOpacity
              onPress={() => {
                this.setState({activeIndex: 0});
                this.setState({showVideo: true});
              }}
              style={
                this.state.activeIndex === 0
                  ? styles.bottonmp3
                  : styles.btnActivemp3
              }>
              <Image
                style={styles.imagemp3}
                source={
                  this.state.activeIndex === 0
                    ? require('../../../assets/images/mp3.png')
                    : require('../../../assets/images/mp3unactive.png')
                }
              />
              <Text
                style={
                  this.state.activeIndex === 0
                    ? styles.textmp3
                    : styles.textmp3nonactive
                }>
                MP3
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{paddingLeft: 15}}>
            <TouchableOpacity
              onPress={() => {
                this.setState({activeIndex: 1});
                this.setState({showVideo: false});
              }}
              style={
                this.state.activeIndex === 0
                  ? styles.bottonmp4
                  : styles.btnActivemp4
              }>
              <Image
                style={styles.imagemp4}
                source={
                  this.state.activeIndex === 0
                    ? require('../../../assets/images/mp4.png')
                    : require('../../../assets/images/mp4active.png')
                }
              />
              <Text
                style={
                  this.state.activeIndex === 0
                    ? styles.textmp4
                    : styles.textmp4aktiv
                }>
                MP4
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.sparator} />
        <View style={styles.ViewSettingDownload}>
          <View style={styles.ViewQuality}>
            <Text style={styles.TextQuality}> Quality</Text>
            <SelectDropdown
              data={quality}
              onSelect={(selectedItem, index) =>
                this.setState({quality: selectedItem.itag})
              }
              defaultButtonText={'Select Quality'}
              buttonTextAfterSelection={(selectedItem, index) => {
                return selectedItem.title;
              }}
              rowTextForSelection={(item, index) => {
                return item.title;
              }}
              buttonStyle={styles.dropdown2BtnStyle}
              buttonTextStyle={styles.dropdown2BtnTxtStyle}
              dropdownIconPosition={'right'}
              dropdownStyle={styles.dropdown2DropdownStyle}
              rowStyle={styles.dropdown2RowStyle}
              rowTextStyle={styles.dropdown2RowTxtStyle}
              renderDropdownIcon={isOpened => {
                return (
                  <Feather
                    name={isOpened ? 'chevron-up' : 'chevron-down'}
                    color={'#8F9BB3'}
                    size={18}
                  />
                );
              }}
            />
          </View>
          <View style={styles.ViewTitle}>
            <Text style={styles.TextTitle}> Title</Text>
            <TextInput
              value={this.state.title}
              TextColor={'#8F9BB3'}
              style={styles.inputTitle}
              onChangeText={e => {
                this.setState({title: e});
              }}
            />
          </View>

          {this.state.showVideo && (
            <View>
              <View style={styles.Viewartist}>
                <Text style={styles.TextArtist}> Artist</Text>
                <TextInput
                  placeholder={this.state.artis}
                  placeholderTextColor={'#8F9BB3'}
                  style={styles.inputArtist}
                />
              </View>
              <TouchableOpacity
                style={styles.buttonDownload}
                onPress={() => this.DownloadMusic()}>
                <Feather name="download" size={20} color="#FFF" />
                <Text style={{color: '#FFF'}}> Download </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>

        {!this.state.showVideo && (
          <View>
            <TouchableOpacity
              style={styles.buttonAdd}
              onPress={() => this.readfolder()}>
              <Feather name="folder-plus" size={20} color="#46A2BC" />
              <Text style={{color: '#46A2BC'}}> Add Folder </Text>
              <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalVisible}>
                <View style={styles.centeredView}>
                  <View style={styles.modalView}>
                    <Image
                      style={styles.imagefoldermodal}
                      source={require('../../../assets/images/folder.png')}
                    />
                    <View>
                      <TextInput
                        placeholder="Input Folder Name"
                        value={this.state.folder}
                        onChangeText={folder => this.setState({folder})}
                        style={styles.placeholdermodal}
                      />
                    </View>
                    <View style={styles.viewcancel}>
                      <TouchableOpacity
                        style={styles.buttoncancel}
                        onPress={() =>
                          this.setModalVisible(!this.state.modalVisible)
                        }>
                        <Text style={styles.textcancel}>Cancel</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={styles.buttonCreate}
                        onPress={() => this.makeDirectory()}>
                        <Text style={styles.textcreate}>Create</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </Modal>
              {/* view rbsheet */}
              <RBSheet
                ref={ref => {
                  this.RBSheet = ref;
                }}
                height={380}
                openDuration={250}>
                <TouchableOpacity style={styles.slideitemsheet} />
                <View style={styles.tampilanutama}>
                  <Text style={styles.textfolder}>Folder</Text>
                  <TouchableOpacity
                    style={styles.iconfoldertambah}
                    onPress={() => {
                      this.setModalVisible(!this.state.modalVisible);
                      this.RBSheet.close();
                    }}>
                    <Feather name="folder-plus" size={25} color="#3A3A3A" />
                  </TouchableOpacity>
                </View>
                <View style={styles.sparator2} />
                <ScrollView>
                  <View style={styles.viewfolder}>{this.list()}</View>
                </ScrollView>
                <View
                  style={{
                    alignItems: 'center',
                    marginTop: 28,
                    marginBottom: 10,
                  }}>
                  <TouchableOpacity
                    style={styles.RBsheetClose}
                    onPress={() => this.RBSheet.close()}>
                    <Text style={styles.textRBCancel}> Cancel </Text>
                  </TouchableOpacity>
                </View>
              </RBSheet>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.buttonDownloadMp4}
              onPress={() => this.cekmedia()}>
              <Feather name="download" size={20} color="#FFF" />
              <Text style={{color: '#FFF'}}> Download </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}
