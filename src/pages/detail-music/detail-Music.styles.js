import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  view1: {
    flex: 1,
    alignItems: 'center',
  },
  viewimage: {
    width: 349,
    height: 405,
    borderRadius: 20,
  },
  viewtext: {
    marginBottom: 48,
    flexDirection: 'row',
  },
  text: {
    color: '#3A3A3A',
    fontSize: 18,
    fontStyle: 'normal',
    fontWeight: '500',
    fontFamily: 'Poppins-Medium',
    width: 283,
    height: 48,
    lineHeight: 24,
  },
  progrressContainer: {
    width: 350,
    bottom: 27,
  },
  progrressLabel: {
    width: 335,
    flexDirection: 'row',
    justifyContent: 'space-between',
    fontSize: 14,
    left: 10,
    bottom: 20,
  },
  progrressLabelText: {
    color: '#6A6A6A',
    paddingHorizontal: 8,
    right: 10,
  },
  Playbutton: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: 280,
    alignContent: 'center',
    bottom: 30,
  },
});
export default styles;
