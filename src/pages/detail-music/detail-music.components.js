import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, SafeAreaView} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import Feather from 'react-native-vector-icons/Feather';
import styles from './detail-Music.styles';
import Slider from '@react-native-community/slider';

export default class Detail_Music extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 0,
      carouselItems: [
        {
          image: require('../../../assets/images/play.png'),
        },
        {
          image: require('../../../assets/images/play.png'),
        },
        {
          image: require('../../../assets/images/play.png'),
        },
        {
          image: require('../../../assets/images/play.png'),
        },
        {
          image: require('../../../assets/images/play.png'),
        },
      ],
    };
  }

  _renderItem({item, index}) {
    return (
      <View>
        <Image style={styles.viewimage} source={item.image} />
      </View>
    );
  }

  handleBackButtonClick() {
    this.props.navigation.goBack(null);
    return true;
  }

  render() {
    return (
      <SafeAreaView style={styles.view1}>
        <TouchableOpacity onPress={() => this.handleBackButtonClick()}>
          <Feather
            name="arrow-left"
            size={24}
            color="#3A3A3A"
            style={{top: 16, right: 165}}
          />
        </TouchableOpacity>
        <View style={{left: 20, flex: 1, top: 33}}>
          <Carousel
            layout={'tinder'}
            ref={ref => (this.carousel = ref)}
            data={this.state.carouselItems}
            sliderWidth={390}
            itemWidth={390}
            renderItem={this._renderItem}
            // layoutCardOffset={'3'}
          />
        </View>
        <View style={styles.viewtext}>
          <Text style={styles.text}>
            The Chainsmokers & Coldplay -Something Just Like This
          </Text>
          <TouchableOpacity style={{paddingLeft: 20}}>
            <Image
              style={{width: 20.9, height: 18.23}}
              source={require('../../../assets/images/like.png')}
            />
          </TouchableOpacity>
        </View>
        <View style={{right: 130, bottom: 45}}>
          <Text>by Coldplay</Text>
        </View>
        <View>
          <Slider
            style={styles.progrressContainer}
            value={10}
            minimumValue={0}
            maximumValue={14}
            thumbTintColor="#FFFFFF"
            minimumTrackTintColor="#46A2BC"
            maximumTrackTintColor="#46A2BC"
            onSlidingComplete={() => {}}
          />
        </View>
        <View style={styles.progrressLabel}>
          <Text style={styles.progrressLabelText}>00:00</Text>
          <Text style={styles.progrressLabelText}>03:15</Text>
        </View>

        <View style={styles.Playbutton}>
          <TouchableOpacity onPress={() => {}}>
            <Feather
              name="shuffle"
              size={20}
              color="#3A3A3A"
              style={{marginTop: 99, right: 83}}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <Feather
              name="skip-back"
              size={20}
              color="#3A3A3A"
              style={{marginTop: 99, right: 31}}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <Image
              source={require('../../../assets/images/buttonplay.png')}
              style={{width: 50, height: 50, marginTop: 83}}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <Feather
              name="skip-forward"
              size={20}
              color="#3A3A3A"
              style={{marginTop: 99, left: 31}}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <Feather
              name="repeat"
              size={20}
              color="#3A3A3A"
              style={{marginTop: 99, left: 83}}
            />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}
