import React from 'react';
import {View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../pages/home/home.components';
import Music from '../pages/music/music.components';
import Video from '../pages/video/video.components';
import Download from '../pages/download/download.components';
import Detail_Music from '../pages/detail-music/detail-music.components';
import Detail_Video from '../pages/detail-video/detail-video.components';
import Feather from 'react-native-vector-icons/Feather';
import styles from './tabnavigation.styles';

const Tab = createBottomTabNavigator();
const MainStack = createStackNavigator();
const HomeStack = createStackNavigator();
const MusicStack = createStackNavigator();
const VideoStack = createStackNavigator();


const homeStack = () => (
  <HomeStack.Navigator
    initialRouteName="Home"
    screenOptions={{headerShown: false}}>
    <HomeStack.Screen name="Home" component={Home} />
   
    {/*add more screen with bottom tabs here*/}
  </HomeStack.Navigator>
);
const videoStack = () => (
  <VideoStack.Navigator
    initialRouteName="Video"
    screenOptions={{headerShown: false}}>
    <VideoStack.Screen name="Video" component={Video} />
    <VideoStack.Screen name="Detail_Video" component={Detail_Video} />
    {/*add more screen with bottom tabs here*/}
  </VideoStack.Navigator>
);
const musicStack = () => (
  <MusicStack.Navigator
    initialRouteName="Music"
    screenOptions={{headerShown: false}}>
    <MusicStack.Screen name="Music" component={Music} />
    {/*add more screen with bottom tabs here*/}
  </MusicStack.Navigator>
);
const tabnavigation = () => {
  return (
    <Tab.Navigator screenOptions={{ 
      headerShown: false, tabBarShowLabel: false, tabBarStyle: {height: 72, ...styles.shadow}}}>
      <Tab.Screen name="HomeStack" component={homeStack} 
      options={{tabBarIcon: ({focused}) => (
        <View style={focused ? styles.Button : styles.Buttonnormal}>
        <Feather name="download" size={24} color={focused ? '#FFF' : '#3A3A3A'}/></View>)}}/>
      <Tab.Screen name="MusicStack" component={musicStack}
      options={{ tabBarIcon: ({focused}) => (
        <View style={focused ? styles.Button : styles.Buttonnormal}>
        <Feather name="music" size={24} color={focused ? '#FFF' : '#3A3A3A'}/></View>)}}/>
      <Tab.Screen name="VideoStack" component={videoStack} 
      options={{ tabBarIcon: ({focused}) => (
        <View style={focused ? styles.Button : styles.Buttonnormal}>
        <Feather name="video" size={24} color={focused ? '#FFF' : '#3A3A3A'}/></View>)}}/>
    </Tab.Navigator>
    )
  };
  
export default function Navigation() {
  return (
    <NavigationContainer>
      <MainStack.Navigator screenOptions={{headerShown: false}}>
        <MainStack.Screen name="Tabnavigation" component={tabnavigation} />
        <MainStack.Screen name="Detail_Music" component={Detail_Music} />
        <MainStack.Screen name="Download" component={Download} />
      </MainStack.Navigator>
    </NavigationContainer>
  );
}
