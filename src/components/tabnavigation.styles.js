import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
    Button: {
        backgroundColor: '#46A2BC',
        width: 42,
        height: 42,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 9,
        paddingVertical: 9,
      },
    
      Buttonnormal: {
        color: '#FFF',
      },
      shadow: {
        color: '#707070',
        shadowOffset: {
          width: 0,
          height: 10,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.5,
        elevation: 5,
      },
  });
  export default styles;